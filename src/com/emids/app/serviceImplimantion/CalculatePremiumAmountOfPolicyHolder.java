package com.emids.app.serviceImplimantion;

import java.util.Map;
import java.util.TreeMap;

import com.emids.dto.personCurentHealth;
import com.emids.dto.personData;
import com.emids.dto.personHabits;




public class CalculatePremiumAmountOfPolicyHolder {

	
	

	public double getAmount(personData personData) {
		double paidAmount = 0;
		paidAmount = percentBasedOnAges(personData.getAge());
		paidAmount = amountbasedOnGender(personData.getGender(), paidAmount);
		paidAmount = getAmuntBasedOnHealthAndHabits(personData.getPersonCurentHealth() ,personData.getPersonHabits(), paidAmount);
		return paidAmount;
	}

	private double percentBasedOnAges(int age) {
		
		double amount = 0;
		amount = age <= 40 ? calculateOnageLimit(age) :calculateOnageLimit(age) + calculateOnageLimit(age) * 0.2;
		return amount;
		
	
	}

	private double amountbasedOnGender(String gender, double amount) {
		
		double tempamount = 0;
		if (gender.equalsIgnoreCase("MALE")|| gender.equalsIgnoreCase("FEMALE")) tempamount = amount + (amount * 0.02);
		return tempamount;
	}

	private double getAmuntBasedOnHealthAndHabits(personCurentHealth personCurentHealth,personHabits personHabits, double amount) {
		if (personCurentHealth.getBloodPlesure().equalsIgnoreCase("YES"))amount+=(amount * 0.01);
		if (personCurentHealth.getBloodSuger().equalsIgnoreCase("YES")) amount+=(amount * 0.01);
		if (personCurentHealth.getHyperTention().equalsIgnoreCase("YES")) amount+=(amount * 0.01);
		if (personCurentHealth.getOverHeight().equalsIgnoreCase("YES")) amount+=(amount * 0.01);
		double temp=amount;
	    if (personHabits.getAlcohal().equalsIgnoreCase("YES")) amount+=(temp * 0.03);
		if (personHabits.getDrugs().equalsIgnoreCase("YES"))amount+=(temp * 0.03);
		if (personHabits.getSmoking().equalsIgnoreCase("YES")) amount+=(temp* 0.03);
		if (personHabits.getDailyExcerises().equalsIgnoreCase("YES")) amount-=(temp* 0.03);
		return amount;
	}
	
	
	
	private static Map<String, Integer> ageLimits = new TreeMap<>();

	static {
		ageLimits.put("18-25", 10);
		ageLimits.put("25-30", 10);
		ageLimits.put("30-35", 10);
		ageLimits.put("35-40", 10);
	}
	

	public double calculateOnageLimit(int enterAge) {
		double amount = 5000;
		int startAge = 0;
		int endAge = 0;
		double perc = 0;
		for (String age : ageLimits.keySet()) {
			startAge = splitAge(age)[0];
			endAge = splitAge(age)[1];
			if ((enterAge >= startAge && enterAge <= endAge)|| (enterAge >= startAge && enterAge >= endAge)) {
				perc = (double) ageLimits.get(age) / 100;
				amount = amount + (amount * perc);
				
			}
		}
		return amount;
	}

	private static int[] splitAge(String age) {
		String[] s = age.split("-");
		return new int[] { Integer.valueOf(s[0]), Integer.valueOf(s[1]) };
	}
}
