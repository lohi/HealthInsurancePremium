package com.emids.dto;

public class personData 
{
private String name;
private int age;
private String gender;
private personCurentHealth personCurentHealth;
private personHabits personHabits;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public personCurentHealth getPersonCurentHealth() {
	return personCurentHealth;
}
public void setPersonCurentHealth(personCurentHealth personCurentHealth) {
	this.personCurentHealth = personCurentHealth;
}
public personHabits getPersonHabits() {
	return personHabits;
}
public void setPersonHabits(personHabits personHabits) {
	this.personHabits = personHabits;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}

}
